from django.db import models

# Create your models here.
class FilterationsForJira(models.Model):

    ButtonName = models.CharField(max_length=500)
    Jql = models.CharField(max_length=1000)
    QuickAccess = models.BooleanField(default=False)



    def __str__(self):
        return self.ButtonName

class SlideShow(models.Model):
    slugid = models.IntegerField(default=0)
    
    def __str__(self):
        return str(self.slugid)
