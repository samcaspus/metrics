from django.shortcuts import render, redirect
import requests
from requests.auth import HTTPBasicAuth
import json
from .models import FilterationsForJira, SlideShow
from datetime import datetime, date
from calendar import monthrange

# Create your views here.
def quarter(startdate="2019-01-01",enddate="2019-03-31",jql=False):
    BASE = "https://hackerearth.atlassian.net/rest/api/3/search?jql="

    if jql!=False:
        url = BASE + jql
    else:
        url = BASE + "issuetype = Bug AND project = REC AND priority in (P0, P1) AND labels = production_bug AND updated >= "+str(startdate)+" AND updated <"+str(enddate)+" ORDER BY priority DESC"
        # url = BASE + "issuetype = Bug AND project = REC AND priority in (P0, P1) AND environment ~ production AND created >= "+str(startdate)+" AND created <= "+str(enddate)+""
    print(url)
    EMAIL = ""
    CYPHER = ""
    auth = HTTPBasicAuth(EMAIL, CYPHER)
    # print(url)
    headers = {
    "Accept": "application/json"
    }

    response = requests.request(
    "GET",
    url,
    headers=headers,
    auth=auth
    )

    dict = json.loads(response.text)
    P0 = 0
    P1 = 0
    P0P1 = 0

    for i in dict["issues"]:
        if (i["fields"]["priority"]["name"])=="P0":
            P0+=1
            P0P1+=1
        elif (i["fields"]["priority"]["name"])=="P1":
            P1+=1
            P0P1+=1
    
    return P0, P1, P0P1


def index(request):
    current_year_full = datetime.now().strftime('%Y') 
    # current_year_full = "2019" 
    
    q1p0,q1p1,q1p0p1 = quarter(current_year_full+"-01-01",current_year_full+"-03-31")
    q2p0,q2p1,q2p0p1 = quarter(current_year_full+"-04-01",current_year_full+"-06-30")
    q3p0,q3p1,q3p0p1 = quarter(current_year_full+"-07-01",current_year_full+"-09-30")
    q4p0,q4p1,q4p0p1 = quarter(current_year_full+"-10-01",current_year_full+"-12-31")
    
    return render(request,"index.html",{
    "year" : current_year_full,
    "q1p0" : q1p0,
    "q1p1" : q1p1,
    "q1p0p1" : q1p0p1,
    "q2p0" : q2p0,
    "q2p1" : q2p1,
    "q2p0p1" : q2p0p1,
    "q3p0" : q3p0,
    "q3p1" : q3p1,
    "q3p0p1" : q3p0p1,
    "q4p0" : q4p0,
    "q4p1" : q4p1,
    "q4p0p1" : q4p0p1,
     })


def slug(requests,slug):
    obj = FilterationsForJira.objects.get(id=slug)
    JQL = obj.Jql
    try:
            
        p0,p1,p0p1 = quarter(jql=JQL)
    except:
        p0 = 0
        p1 = 0
        p0p1 = 0

    
    if p0==p1==p0p1==0:
        return render(requests,"viewChart.html",{
            "data": False,
            "ButtonName" : obj.ButtonName,
            "id" : slug,
        })

    return render(requests,"viewChart.html",{
        "q1p0" : p0,
        "q1p1" : p1,
        "q1p0p1" : p0p1,
        "ButtonName" : obj.ButtonName,
        "id" : slug,
        "data" : True
    })

def addFilters(requests):
    return render(requests,"addFilter.html",{})


def viewFilters(requests):
    obj = FilterationsForJira.objects.all()

    return render(requests,"viewFilter.html",{"filters": obj})

def add(requests):
    ButtonName = requests.POST["ButtonName"]
    Jql = requests.POST["ButtonJql"]
    obj = FilterationsForJira(ButtonName=ButtonName,Jql=Jql,QuickAccess=False)
    obj.save()
    return render(requests,"addFilter.html",{"message" : "added successfuly"})


def deleteChart(requests):
    id = requests.POST["chartIdentity"]
    obj = FilterationsForJira.objects.get(id = id)
    print(obj)
    obj.delete()
    return viewFilters(requests)

def viewSlideShow(requests,slug):
    obj = SlideShow.objects.find(slugid=slug)
    pass

def customDashBoard(requests):
    return render(requests,"customDashBoard.html",{})

def getMonth(day):
    month = {
        1: "Jan",
        2 : "Feb",
        3: "Mar",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "Aug",
        9: "Sept",
        10:"Oct",
        11:"Nov",
        12:"Dec"
    }
    return month[day]

def showCustomDashBoard(requests):
    todays_date = str(date.today()).split("-")
    startMonth = int(todays_date[1])
    startYear = int(todays_date[0])
    endMonth = int(todays_date[1])+1
    endYear = int(todays_date[0])
    
    if endMonth==13:
        endMonth = 1
        endYear +=1
        

    print(todays_date)
    days= monthrange(endYear,endMonth)[1]
    p0,p1,p0p1 = quarter(str(startYear)+"-"+str(startMonth)+"-1",str(endYear)+"-"+str(endMonth)+"-"+str(days))

    

    return render(requests,"showCustomDashBoard.html",{
        "q1p0": p0,
        "q1p1" : p1,
        "q1p0p1" : p0p1,
        "startMonth" : getMonth(startMonth),
        "startYear" : startYear,
        "endMonth" : getMonth(endMonth),
        "endYear" : endYear,  
    })

def showCustomDashBoardSlugged(requests,slug):
    todays_date = slug.split("-")
    startMonth = int(todays_date[1])
    startYear = int(todays_date[0])
    endMonth = int(todays_date[1])+1
    endYear = int(todays_date[0])

    if endMonth==13:
        endMonth = 1
        endYear +=1

    
    print(todays_date)
    days= monthrange(endYear,endMonth)[1]
    p0,p1,p0p1 = quarter(str(startYear)+"-"+str(startMonth)+"-1",str(endYear)+"-"+str(endMonth)+"-1")

    

    return render(requests,"showCustomDashBoard.html",{
        "q1p0": p0,
        "q1p1" : p1,
        "q1p0p1" : p0p1,
        "startMonth" : getMonth(startMonth),
        "startYear" : startYear,
        "endMonth" : getMonth(endMonth),
        "endYear" : endYear,  
    })

def generateGraph(requests):    
    
    year = requests.POST["year"]
    month = requests.POST["month"]
    
    return redirect('../CustomDashBoard/'+year+"-"+month+"-1")
    

