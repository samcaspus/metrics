from django.contrib import admin
from .models import FilterationsForJira, SlideShow

# Register your models here.
admin.site.register(FilterationsForJira)
admin.site.register(SlideShow)
