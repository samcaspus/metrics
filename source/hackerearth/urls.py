from django.contrib import admin
from django.urls import path,include
from . import views

urlpatterns = [

    path('add',views.add, name="addElement"),
    path('addFilters',views.addFilters, name="addFilters"),
    path('viewFilters',views.viewFilters, name="viewFilters"),
    
    # path('CustomDashBoard',views.customDashBoard, name="CustomDashBoard"),
    path('CustomDashBoard',views.showCustomDashBoard, name="CustomDashBoard"),
    path('generateGraph',views.generateGraph,name="GenerateGraph"),
    path('CustomDashBoard/generateGraph',views.generateGraph, name="CustomDashBoardCustomised"),
    
    path('CustomDashBoard/<slug:slug>',views.showCustomDashBoardSlugged, name="CustomDashBoardCustomised"),
    
    # path('showCustomDashBoard',views.showCustomDashBoard, name="showCustomDashBoard"),
    
    path('createSlideShow',views.slug, name="addFilters"),
    
    path('startSlideShow',views.slug, name="viewSlideShow"),

    path('startSlideShow/<slug:slug>',views.viewSlideShow, name="viewFilters"),
    
    
    path('charts/view/deleteChart',views.deleteChart, name="viewFilters"),
    
    path('charts/view/<slug:slug>',views.slug, name="slugpage"),
    path('',views.index, name="index_page"),

    
    
]
